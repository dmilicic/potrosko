package com.potrosko;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import android.annotation.TargetApi;
import android.app.ListActivity;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;

import com.potrosko.controller.DataManager;
import com.potrosko.controller.LogFilter;
import com.potrosko.log.model.AbstractLog;
import com.potrosko.tariff.calculator.Calculator;
import com.potrosko.tariff.model.Tariff;

public class RecommenderActivity extends ListActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Show the Up button in the action bar.
		setupActionBar();
		init();
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.recommender, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void init() {
		
        // Create a progress bar to display while the list loads
        ProgressBar progressBar = new ProgressBar(this);
        progressBar.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT));     
        progressBar.setIndeterminate(true);
        getListView().setEmptyView(progressBar);

        // Must add the progress bar to the root of the layout
        ViewGroup root = (ViewGroup) findViewById(android.R.id.content);
        root.addView(progressBar);     
        
        // start calculating other tariff costs in a different thread
        TariffRecommenderTask task = new TariffRecommenderTask();
        task.execute();
	}

	
	private class TariffRecommenderTask extends AsyncTask<Void, Void, List<HashMap<String, Object>>> {

		
		/** Class that represents a recommended tariff */
		private class RecommendedTariffItem {
			
			public static final String NAME = "name";
			public static final String COST = "cost";
			
			private final String name;
			private final double cost;
			
			public RecommendedTariffItem(String name, double cost) {
				this.name = name;
				this.cost = cost;
			}

			public String getName() {
				return name;
			}

			public double getCost() {
				return cost;
			}
		}
		
		
		private void setupListAdapter(List<HashMap<String, Object>> result) {
	        
	        // parameters for the list adapter
	        String[] from = new String[] { RecommendedTariffItem.NAME, RecommendedTariffItem.COST };
	        int[] to = new int[] {R.id.recommendedTariffName, R.id.recommendedTariffCost };	  
	        
	        // setup the list adapter
			SimpleAdapter adapter = null;
			adapter = new SimpleAdapter(RecommenderActivity.this, result, 
					R.layout.list_recommended_item, from, to);
			setListAdapter(adapter);
		}

		@Override
		protected List<HashMap<String, Object>> doInBackground(Void... params) {
	        
	        // calculate the cost of the current tariff
	        double[] costs = Calculator.calculateCosts(DataManager.getCurrentTariff());
	        double totalCost = costs[costs.length - 1];
			
	        // sortable list of cheaper tariffs
	        ArrayList<RecommendedTariffItem> recommendedTariffs = 
	        		new ArrayList<RecommendedTariffItem>();
	        
	        // list needed by the listview adapter
	        List<HashMap<String, Object>> fillMaps = new ArrayList<HashMap<String, Object>>();
	        
	        //get this month identifier
	        int month = Calendar.getInstance().get(Calendar.MONTH);
	        ArrayList<AbstractLog> callLogs = LogFilter.filterByMonth(DataManager.getCallLogs(), month);
	        ArrayList<AbstractLog> smsLogs = LogFilter.filterByMonth(DataManager.getSmsLogs(), month);
	        
	        // get all the tariffs available
	        ArrayList<Object> tariffs = DataManager.getTariffs();
	        double[] tmpCosts;
	        for (int i = 0; i < tariffs.size(); i++) {
	        	
	        	// get the next tariff and calculate its cost
	        	Tariff tmpTariff = (Tariff) tariffs.get(i);
	        	tmpCosts = Calculator.calculateCosts(tmpTariff, callLogs, smsLogs);
	        	
	        	System.out.println(tmpTariff.toJson());
	        	
	        	Log.d("TARIFA", tmpTariff.getNAME());
	        	Log.d("mjesecno", String.valueOf(tmpCosts[0]));
	        	Log.d("internet", String.valueOf(tmpCosts[1]));
	        	Log.d("pozivi", String.valueOf(tmpCosts[2]));
	        	Log.d("sms", String.valueOf(tmpCosts[3]));
	        	Log.d("ukupno", String.valueOf(tmpCosts[4]));
//	        	Log.d("tmpCost", String.valueOf(tmpCosts[tmpCosts.length - 1]));	        
	        	
	        	// check if the tested tariff is less expensive than the current,
	        	// if it is then add it to the list
	        	if (totalCost < tmpCosts[tmpCosts.length - 1])
	        		continue;
	        	
	        	recommendedTariffs.add(new RecommendedTariffItem(tmpTariff.getNAME(), 
	        							 tmpCosts[tmpCosts.length - 1]));        
	    	} 
	        
	        // sort the tariffs by cost - from cheaper to more expensive
	        Collections.sort(recommendedTariffs, new Comparator<RecommendedTariffItem>() {
				@Override
				public int compare(RecommendedTariffItem lhs,
						RecommendedTariffItem rhs) {
					if (lhs.getCost() > rhs.cost)
						return 1;
					if (lhs.getCost() < rhs.cost)
						return -1;
					return 0;
				}
			});
	        
	        // put recommended tariffs in a map
	        for(int i = 0; i < recommendedTariffs.size(); i++) {
    	        RecommendedTariffItem item = recommendedTariffs.get(i);
				// add this new recommended tariff to the list
				HashMap<String, Object> map = new HashMap<String, Object>();  
				map.put(RecommendedTariffItem.NAME, item.getName());
				map.put(RecommendedTariffItem.COST, DataManager.addCurrency(
												    DataManager.format(item.getCost())));
				fillMaps.add(map);	
	        }
	        
	        return fillMaps;
		}
		
		@Override
		protected void onPostExecute(List<HashMap<String, Object>> result) {
			super.onPostExecute(result);
			setupListAdapter(result);
		}
	}

}
