package com.potrosko;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.potrosko.controller.DataManager;
import com.potrosko.tariff.model.Tariff;

public class TariffsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tariffs);
		init();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tariffs, menu);
		return false;
	}

	public void init() {		
		
		ArrayList<String> tariffs = new ArrayList<String>();
		ListView listView = (ListView) findViewById(R.id.tariff_list);
		
		String operator = getIntent().getStringExtra("Operator");
		
		//get all the tariffs from this specific operator
		ArrayList<Object> parsedObjects = null;
		parsedObjects = DataManager.getTariffs(operator);

		
		//make objects final for the inner class
		final ArrayList<Object> objects = parsedObjects;
		
		for (int i = 0; i < objects.size(); i++) {
			Tariff tariff = (Tariff) objects.get(i);			
			tariffs.add(tariff.getNAME());
		}
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, tariffs);
		listView.setAdapter(adapter);
			
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				
				Tariff tariff = (Tariff) objects.get(arg2);
				Tariff.currentTariff = tariff;
								
//				Toast.makeText(getApplicationContext(), Tariff.currentTariff.getNAME(), Toast.LENGTH_SHORT).show();
				
				//save the currently active tariff name to shared preferences
				SharedPreferences prefs = getSharedPreferences(DataManager.PREFS_NAME, 0);
				Editor editor = prefs.edit();
				editor.putString(DataManager.PREFS_TARIFF_NAME, tariff.getNAME());
				editor.commit();
				
				//save the currently active tariff to internal storage
				saveTariffData(tariff);
				
				//after selecting the tariff, start calculating the costs on the main activity
				Intent main = new Intent(getApplicationContext(), MainActivity.class);
				startActivity(main);
				
				//kill this activity
				TariffsActivity.this.finish();
			}
		});
	}
	
	private void saveTariffData(Tariff tariff) {
		DataManager.storeData(tariff.toJson().toString(), DataManager.FILE_ACTIVE_TARIFF);	
		DataManager.storeFreeCallData(tariff.getFreeCalls());
		DataManager.storeFreeSmsData(tariff.getFreeSms());
	}
}

