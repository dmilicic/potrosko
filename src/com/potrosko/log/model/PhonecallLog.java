package com.potrosko.log.model;

public class PhonecallLog extends AbstractLog {

	private String numberDialed;
	private int duration;
	
	public PhonecallLog(String numberDialed, long date, int duration) {
		this.numberDialed = numberDialed;
		this.date = date;
		this.duration = duration;
	}

	public String getNumberDialed() {
		return numberDialed;
	}

	/** Gets the duration of the call in seconds. */
	public int getDuration() {
		return duration;
	}	
	
	@Override
	public boolean equals(Object o) {
		PhonecallLog log = (PhonecallLog) o;
		return (log.getDate() == this.getDate() && 
				log.getDuration() == this.getDuration() && 
				log.getNumberDialed().equals(this.getNumberDialed()));
	}
}
