package com.potrosko.log.model;


public class SmsLog extends AbstractLog  {
	
	private String numberSent; 
	
	public SmsLog(String numberSent, long dateSent) {
		this.numberSent = numberSent;
		this.date = dateSent;
	}

	public String getNumberSent() {
		return numberSent;
	}
	
	@Override
	public boolean equals(Object o) {
		SmsLog log = (SmsLog) o;
		return ( log.getDate() == this.date && log.getNumberSent() == this.numberSent );
	}
}
