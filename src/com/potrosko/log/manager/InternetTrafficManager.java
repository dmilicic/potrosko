package com.potrosko.log.manager;

import java.util.Calendar;

import android.content.SharedPreferences;
import android.net.TrafficStats;
import android.util.Log;

import com.potrosko.controller.DataManager;

public class InternetTrafficManager {
	
	private static final int MEGABTYES = 1000000;
	
	private static InternetTrafficManager instance;
	
	private InternetTrafficManager() {}
	
	public static InternetTrafficManager getInstance() {
		if(instance == null) {
			instance = new InternetTrafficManager();
		} 
		
		return instance;
	}
	
	/** Return the total Internet traffic for the current month */
	public long getMonthlyTraffic() {
//		long total = TrafficStats.getMobileTxBytes() / MEGABTYES + 
//				 	 TrafficStats.getMobileRxBytes() / MEGABTYES;
		
		Calendar cal = Calendar.getInstance();
		String month = String.valueOf(cal.get(Calendar.MONTH));	
		
		SharedPreferences prefs = DataManager.context.getSharedPreferences(DataManager.PREFS_NAME, 0);		
		long total = prefs.getLong(month, 0);
		
//		Log.d("MONTHLY INTERNET", String.valueOf(total));
	
		return total;
	}
	
	public static double toMegabytes(long bytes) {
		return bytes * 1.0 / MEGABTYES;
	}
	
	/** Gets the total number of transmitted and received bytes through mobile interfaces */
	public static long getTotalBytes() {
		return getTxBytes() + getRxBytes();
	}
	
	/** Gets the total number of transmitted bytes through mobile interfaces */
	public static long getTxBytes() {
		return TrafficStats.getMobileTxBytes();
	}

	/** Gets the total number of received bytes through mobile interfaces */
	public static long getRxBytes() {
		return TrafficStats.getMobileRxBytes();
	}
	
	/** Returns how much megabytes were spent by user at the start of the day */
	public static long getTotalStartBytes() {
		return getStartTxBytes() + getStartRxBytes();
	}
	
	public static long getStartTxBytes() {
		SharedPreferences prefs = DataManager.context.getSharedPreferences(DataManager.PREFS_NAME, 0);
		return prefs.getLong(DataManager.PREFS_START_TX_BYTES, 0);
	}

	public static long getStartRxBytes() {
		SharedPreferences prefs = DataManager.context.getSharedPreferences(DataManager.PREFS_NAME, 0);
		return prefs.getLong(DataManager.PREFS_START_RX_BYTES, 0);
	}
}
