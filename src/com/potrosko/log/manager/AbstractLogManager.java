package com.potrosko.log.manager;

import java.util.ArrayList;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.potrosko.log.model.AbstractLog;

public abstract class AbstractLogManager implements ILogManager {
	
	private static final String REGION = "HR";
	
	private ArrayList<AbstractLog> logList;

	/**
	 * Gets all the logs available in the phone
	 * @return
	 */
	protected abstract ArrayList<AbstractLog> getLogs();
	
	/** Formalizes the given number to the format [country code] + [national number], eg. from 12345789 to 385-123456789 */
	public static String formalizePhoneNumber(String num) 
			throws NumberParseException {
		
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		
		//formalize the number
		PhoneNumber numberProto = null;
		  numberProto = phoneUtil.parse(num, REGION);
		  num = String.valueOf(numberProto.getCountryCode()) +
    		  	String.valueOf(numberProto.getNationalNumber());
		  
		return num;
	}
	
	@Override
	public void updateLogs() {
		ArrayList<AbstractLog> newLogs = getLogs();
		for (AbstractLog log : newLogs) {
			if(!logList.contains(log)) {
				logList.add(log);
			}
		}
	}

	/**
	 * Gets the logs that are currently stored in apps memory buffer
	 * @return
	 */
	public ArrayList<AbstractLog> getLogList() {
		return logList;
	}
}
