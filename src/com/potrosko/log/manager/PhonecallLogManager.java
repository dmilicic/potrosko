package com.potrosko.log.manager;

import java.util.ArrayList;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.CallLog;

import com.google.i18n.phonenumbers.NumberParseException;
import com.potrosko.log.model.AbstractLog;
import com.potrosko.log.model.PhonecallLog;

public class PhonecallLogManager extends AbstractLogManager {
	
	private static final String[] callProjection = { CallLog.Calls.NUMBER, 
													 CallLog.Calls.DURATION,
													 CallLog.Calls.DATE
												};
	
	private static final String callSelection = CallLog.Calls.TYPE + "=?";
	
	private static final String[] callSelectionArgs = { Integer.toString(CallLog.Calls.OUTGOING_TYPE) };
	
	private ContentResolver contentResolver;
	
	public PhonecallLogManager(ContentResolver contentResolver) {
		this.contentResolver = contentResolver;
	}
	
	@Override
	public ArrayList<AbstractLog> getLogs() {
		ArrayList<AbstractLog> resultCallLogs = new ArrayList<AbstractLog>();
		
		Cursor cursor = contentResolver.query(CallLog.Calls.CONTENT_URI, 
											  callProjection, 
											  callSelection, 
											  callSelectionArgs, 
											  CallLog.Calls.DATE + " DESC");
		cursor.moveToFirst();
		
		do {
			String num = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
			int duration = cursor.getInt(cursor.getColumnIndex(CallLog.Calls.DURATION));
			long date = cursor.getLong(cursor.getColumnIndex(CallLog.Calls.DATE));
			
			try {
				num = formalizePhoneNumber(num);
				resultCallLogs.add(new PhonecallLog(num, date, duration));
			} catch (NumberParseException e) {
				e.printStackTrace();
			}			
		} while (cursor.moveToNext());	
		cursor.close();
		
		return resultCallLogs;
	}

	@Override
	public void updateDatabase() {

	}
}
