package com.potrosko.log.manager;

public interface ILogManager {
	
	/**
	 * Updates the log buffer with new entries
	 */
	public void updateLogs();
	
	/**
	 * Updates the log database with new entries 
	 */
	public void updateDatabase();
}
