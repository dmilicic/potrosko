package com.potrosko.log.manager;

import java.util.ArrayList;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import com.google.i18n.phonenumbers.NumberParseException;
import com.potrosko.log.model.AbstractLog;
import com.potrosko.log.model.SmsLog;

public class SmsLogManager extends AbstractLogManager {

	private static final String SMS_CONTENT_URI = "content://sms/sent";
	
	private static final String SMS_NUMBER_SENT = "address";
	
	private static final String SMS_DATE_SENT = "date";
	
	private static final String[] smsProjection = { SMS_NUMBER_SENT, SMS_DATE_SENT };
	
	private ContentResolver contentResolver;
	
	
	public SmsLogManager(ContentResolver contentResolver) {
		this.contentResolver = contentResolver;
	}
	
	@Override
	public ArrayList<AbstractLog> getLogs() {
		ArrayList<AbstractLog> resultSmsLogs = new ArrayList<AbstractLog>();		
				
		Cursor cursor = contentResolver.query(Uri.parse(SMS_CONTENT_URI), smsProjection, null, null, null);
		cursor.moveToFirst();
		do {
			int addressIdx = cursor.getColumnIndex(SMS_NUMBER_SENT);
			int dateIdx = cursor.getColumnIndex(SMS_DATE_SENT);
			
			String num = cursor.getString(addressIdx);
			long date = cursor.getLong(dateIdx);
			
			try {
				num = formalizePhoneNumber(num);
				SmsLog log = new SmsLog(num, date);
				resultSmsLogs.add(log);
			} catch (NumberParseException e) {
				e.printStackTrace();
			}
		} while( cursor.moveToNext() );
		cursor.close();

		return resultSmsLogs;
	}

	@Override
	public void updateDatabase() {
		// TODO Auto-generated method stub

	}



}
