package com.potrosko;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.potrosko.controller.DataManager;
import com.potrosko.controller.asynctasks.TariffDownloadTask;
import com.potrosko.tariff.model.Tariff;

public class OperatorsActivity extends Activity {

	private ArrayList<Object> objects;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_operators);
				
		//clear shared prefs
//		SharedPreferences prefs = getSharedPreferences(DataManager.PREFS_NAME, 0);
//		prefs.edit().clear().commit();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		init();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.operators, menu);
		return false;
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		objects = null;
	}
	
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager 
              = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
	
	public void init() {
		DataManager.context = this;
		
		try {
			
			//preload some number-operator information
			if(isNetworkAvailable()) {
				
				//downloads new tariff information
				TariffDownloadTask task = DataManager.downloadTariffs();
											
				DataManager.updateNumberOperatorMap(DataManager.getAllNumbers());
				
				//wait for the new tariff information to load
				task.get();
				objects = DataManager.getTariffs();
			} else {
				Log.d("POTROSKO", "nema interneta!");
				
				//load tariffs from memory or assets file
				objects = DataManager.getTariffs();
			}
 
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
				

		final Context context = getApplicationContext();
		
		ArrayList<String> operators = new ArrayList<String>();
		ListView listView = (ListView) findViewById(R.id.operator_list);
		
		for (int i = 0; i < objects.size(); i++) {
			Tariff tariff = (Tariff) objects.get(i);
			if(operators.contains(tariff.getOPERATOR()))
				continue;
			operators.add(tariff.getOPERATOR());
		}
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, operators);
		listView.setAdapter(adapter);
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int idx,
					long arg3) {
				
				
//		        SharedPreferences settings = getSharedPreferences(DataManager.PREFS_NAME, 0);
//		        SharedPreferences.Editor editor = settings.edit();
//		        editor.putBoolean(DataManager.PREFS_FIRST_RUN, false);
//		        editor.commit();
				
				//after picking your operator, start an activity for picking the tariffs
				Intent tariffs = new Intent(context, TariffsActivity.class);
				tariffs.putExtra("Operator", adapter.getItemAtPosition(idx).toString());
				startActivity(tariffs);
				
				//kill this activity
				OperatorsActivity.this.finish();
			}
		});
	}

}
