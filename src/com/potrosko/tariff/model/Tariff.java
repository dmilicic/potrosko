package com.potrosko.tariff.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.potrosko.controller.DataManager;

public class Tariff {
	
	// ---- JSON data -----
	
	private final static String JSON_OPERATOR = "Operator";
	private final static String JSON_TYPE = "Type";
	private final static String JSON_NAME = "Name";
	private final static String JSON_MONTHLY_COST = "Monthly cost";
	private final static String JSON_CALL_COST = "Call cost";
	private final static String JSON_CALL_START_COST = "Call start cost";
	private final static String JSON_CALL_CALCULATION_UNIT = "Call calculation unit"; //in seconds
	private final static String JSON_SMS_COST = "SMS cost";
	private final static String JSON_MMS_COST = "MMS cost";
	private final static String JSON_INTERNET_TRAFFIC_COST = "Internet traffic cost";
	private final static String JSON_INTERNET_CALCULATION_UNIT = "Internet calculation unit"; //in kilobytes
	private final static String JSON_ACTIVATION_COST = "Activation cost";
	private final static String JSON_FREE_CALL_NETWORKS = "Free call networks";
	private final static String JSON_FREE_SMS_NETWORKS = "Free sms networks";
	private final static String JSON_FREE_CALL_MINUTES = "Free call minutes";
	private final static String JSON_FREE_SMS_COUNT = "Free sms count";
	private final static String JSON_FREE_INTERNET_TRAFFIC = "Free internet traffic";
	private final static String JSON_DISCOUNT = "Discount";
	
	//current active tariff for this phone
	public static Tariff currentTariff;
	
	private final String OPERATOR;
	private final String TYPE;
	private final String NAME;
	
	// ----- FEES -------------

	private double monthlyCost;

	/** Call cost in format cost/minute. */
	private double callCost;
	
	/** Cost for starting a phone call. */
	private double callStartCost;
	
	/** How many seconds will pass before a new call cost will tick */
	private int callCalculationUnit;
	
	private double smsCost;
	private double mmsCost;
	
	private double internetTrafficCost;
	
	/** Internet traffic cost per number of kilobytes */
	private int internetCalculationUnit;
	
	// ------ OTHER COSTS ---------
	
	private double activationCost;
	
	// ------ FREE items ----------
	
	private String[] freeCallNetworks;
	private String[] freeSmsNetworks;
	
	/**
	 * Amount of free calling minutes
	 */
	private double freeCalls;
	
	/**
	 * Amount of free sms
	 */
	private int freeSms;
	
	/**
	 * Amount of free internet traffic
	 */
	private double freeInternetTraffic;
	
	/**
	 * Discount percentage
	 */
	private double discount;
	
	public Tariff(TariffBuilder builder) {
		OPERATOR = builder.OPERATOR;
		TYPE = builder.TYPE;
		NAME = builder.NAME;
		this.monthlyCost = builder.monthlyCost;
		this.callCost = builder.callCost;
		this.callStartCost = builder.callStartCost;
		this.callCalculationUnit = builder.callCalculationUnit;
		this.smsCost = builder.smsCost;
		this.mmsCost = builder.mmsCost;
		this.internetTrafficCost = builder.internetTrafficCost;
		this.internetCalculationUnit = builder.internetCalculationUnit;
		this.freeCallNetworks = builder.freeCallNetworks;
		this.freeSmsNetworks = builder.freeSmsNetworks;
		this.freeCalls = builder.freeCalls;
		this.freeSms = builder.freeSms;
		this.freeInternetTraffic = builder.freeInternetTraffic;
		this.discount = builder.discount;
	};

	public JSONObject toJson() {
		JSONObject object = new JSONObject();
        try {
        	object.put(JSON_OPERATOR, OPERATOR);
        	object.put(JSON_TYPE, TYPE);
        	object.put(JSON_NAME, NAME);
        	object.put(JSON_MONTHLY_COST, monthlyCost);
        	object.put(JSON_CALL_COST, callCost);
        	object.put(JSON_CALL_START_COST, callStartCost);
        	object.put(JSON_CALL_CALCULATION_UNIT, callCalculationUnit);
        	object.put(JSON_SMS_COST, smsCost);
        	object.put(JSON_MMS_COST, mmsCost);
        	object.put(JSON_INTERNET_TRAFFIC_COST, internetTrafficCost);
        	object.put(JSON_INTERNET_CALCULATION_UNIT, internetCalculationUnit);
        	object.put(JSON_ACTIVATION_COST, activationCost);
        	
        	JSONArray freeCallNetworksArr = new JSONArray();
        	for (int i = 0; i < freeCallNetworks.length; i++) {
        		freeCallNetworksArr.put(freeCallNetworks[i]);
			}
        	object.put(JSON_FREE_CALL_NETWORKS, freeCallNetworksArr);
        	
        	
        	JSONArray freeSmsNetworksArr = new JSONArray();
        	for (int i = 0; i < freeSmsNetworks.length; i++) {
        		freeSmsNetworksArr.put(freeSmsNetworks[i]);
			}
        	object.put(JSON_FREE_SMS_NETWORKS, freeSmsNetworksArr);
        	
        	object.put(JSON_FREE_CALL_MINUTES, freeCalls);
        	object.put(JSON_FREE_SMS_COUNT, freeSms);
        	object.put(JSON_FREE_INTERNET_TRAFFIC, freeInternetTraffic);
        	object.put(JSON_DISCOUNT, discount);
         } catch (JSONException e) {
           e.printStackTrace();
         }
  
          return object;
	}
	
	/** Instantiates the tarriff class from a JSON object */
	public static Tariff fromJson(JSONObject obj) {
		String OPERATOR = "";
		String TYPE = "";
		String NAME = "";
		double monthlyCost = 0;
		double callCost = 0;
		double callStartcost = 0;
		int callCalculationUnit = 60;
		double smsCost = 0;
		double mmsCost = 0;
		double internetTrafficCost = 0;
		int internetCalculatonUnit = 1000; 
		double activationCost = 0;
		String[] freeCallNetworks = new String[0];
		String[] freeSmsNetworks = new String[0];
		double freeCallMinutes = 0;
		int freeSmsCount = 0;
		double freeInternetTraffic = 0;
		double discount = 0;

		try {
			OPERATOR = obj.getString(JSON_OPERATOR);
		} catch (JSONException e) {
			Log.d("JSON", e.getMessage());
		}
		
		try {
			TYPE = obj.getString(JSON_TYPE);
		} catch (JSONException e) {
			Log.d("JSON", e.getMessage());
		}
		
		try {
			NAME = obj.getString(JSON_NAME);
		} catch (JSONException e) {
			Log.d("JSON", e.getMessage());
		}
		
		try {
			monthlyCost = obj.getDouble(JSON_MONTHLY_COST);
		} catch (JSONException e) {
			Log.d("JSON", e.getMessage());
		}
		
		try {
			callCost = obj.getDouble(JSON_CALL_COST);
		} catch (JSONException e) {
			Log.d("JSON", e.getMessage());
		}
		
		try {
			callStartcost = obj.getDouble(JSON_CALL_START_COST);
		} catch (JSONException e) {
			Log.d("JSON", e.getMessage());
		}
		
		try {
			callCalculationUnit = obj.getInt(JSON_CALL_CALCULATION_UNIT);
		} catch (JSONException e) {
			Log.d("JSON", e.getMessage());
		}
		
		try {
			smsCost = obj.getDouble(JSON_SMS_COST);
		} catch (JSONException e) {
			Log.d("JSON", e.getMessage());
		}
		
		try {
			mmsCost = obj.getDouble(JSON_MMS_COST);
		} catch (JSONException e) {
			Log.d("JSON", e.getMessage());
		}
		
		try {
			internetTrafficCost = obj.getDouble(JSON_INTERNET_TRAFFIC_COST);
		} catch (JSONException e) {
			Log.d("JSON", e.getMessage());
		}
		
		try {
			internetCalculatonUnit = obj.getInt(JSON_INTERNET_CALCULATION_UNIT);
		} catch (JSONException e) {
			Log.d("JSON", e.getMessage());
		}
		
		try {
			activationCost = obj.getInt(JSON_ACTIVATION_COST);
		} catch (JSONException e) {
			Log.d("JSON", e.getMessage());
		}
		
		try {
			JSONArray arr = new JSONArray();
			arr = obj.getJSONArray(JSON_FREE_CALL_NETWORKS);
			freeCallNetworks = new String[arr.length()];
			for (int i = 0; i < freeCallNetworks.length; i++) {
				freeCallNetworks[i] = arr.getString(i);
			}
		} catch (JSONException e) {
			Log.d("JSON", e.getMessage());
		}
		
		try {
			JSONArray arr = new JSONArray();
			arr = obj.getJSONArray(JSON_FREE_SMS_NETWORKS);
			freeSmsNetworks = new String[arr.length()];
			for (int i = 0; i < freeSmsNetworks.length; i++) {
				freeSmsNetworks[i] = arr.getString(i);
			}
		} catch (JSONException e) {
			Log.d("JSON", e.getMessage());
		}
			
		try {
			freeCallMinutes = obj.getDouble(JSON_FREE_CALL_MINUTES);
		} catch (JSONException e) {
			Log.d("JSON", e.getMessage());
		}
		
		try {
			freeSmsCount = obj.getInt(JSON_FREE_SMS_COUNT);
		} catch (JSONException e) {
			Log.d("JSON", e.getMessage());
		}
		
		try {
			freeInternetTraffic = obj.getDouble(JSON_FREE_INTERNET_TRAFFIC);
		} catch (JSONException e) {
			Log.d("JSON", e.getMessage());
		}
		
		try {
			discount = obj.getDouble(JSON_DISCOUNT);
		} catch (JSONException e) {
			Log.d("JSON", e.getMessage());
		}
		
		TariffBuilder builder = new TariffBuilder(OPERATOR, TYPE, NAME);
		Tariff tariff = builder.setMonthlyCost(monthlyCost)
							   .setCallCost(callCost)
							   .setCallStartCost(callStartcost)
							   .setCallCalculationUnit(callCalculationUnit)
							   .setSmsCost(smsCost)
							   .setMmsCost(mmsCost)
							   .setInternetTrafficCost(internetTrafficCost)
							   .setInternetCalculationUnit(internetCalculatonUnit)
							   .setActivationCost(activationCost)
							   .setFreeCallNetworks(freeCallNetworks)
							   .setFreeSmsNetworks(freeSmsNetworks)
							   .setFreeCalls(freeCallMinutes)
							   .setFreeSms(freeSmsCount)
							   .setFreeInternetTraffic(freeInternetTraffic)
							   .setDiscount(discount)
							   .build();
		
		currentTariff = tariff;
		
		return tariff;
	}

	public double getMonthlyCost() {
		return monthlyCost;
	}

	public void setMonthlyCost(double monthlyCost) {
		this.monthlyCost = monthlyCost;
	}

	public double getCallCost() {
		return callCost;
	}

	public void setCallCost(double callCost) {
		this.callCost = callCost;
	}

	public double getCallStartCost() {
		return callStartCost;
	}

	public void setCallStartCost(double callStartCost) {
		this.callStartCost = callStartCost;
	}

	public double getSmsCost() {
		return smsCost;
	}

	public void setSmsCost(double smsCost) {
		this.smsCost = smsCost;
	}

	public double getInternetTrafficCost() {
		return internetTrafficCost;
	}

	public void setInternetTrafficCost(double internetTrafficCost) {
		this.internetTrafficCost = internetTrafficCost;
	}

	public String[] getFreeCallNetworks() {
		return freeCallNetworks;
	}

	public void setFreeCallNetworks(String[] freeCallNetworks) {
		this.freeCallNetworks = freeCallNetworks;
	}

	public String[] getFreeSmsNetworks() {
		return freeSmsNetworks;
	}

	public void setFreeSmsNetworks(String[] freeSmsNetworks) {
		this.freeSmsNetworks = freeSmsNetworks;
	}

	public double getFreeCalls() {
		return freeCalls;
	}

	public void setFreeCalls(double freeCalls) {
		this.freeCalls = freeCalls;
	}

	public int getFreeSms() {
		return freeSms;
	}

	public void setFreeSms(int freeSms) {
		this.freeSms = freeSms;
	}

	public double getFreeInternetTraffic() {
		return freeInternetTraffic;
	}

	public void setFreeInternetTraffic(double freeInternetTraffic) {
		this.freeInternetTraffic = freeInternetTraffic;
	}
	
	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public String getOPERATOR() {
		return OPERATOR;
	}

	public String getTYPE() {
		return TYPE;
	}

	public String getNAME() {
		return NAME;
	}
	
	public int getCallCalculationUnit() {
		return callCalculationUnit;
	}

	public void setCallCalculationUnit(int callCalculationUnit) {
		this.callCalculationUnit = callCalculationUnit;
	}

	public double getMmsCost() {
		return mmsCost;
	}

	public void setMmsCost(double mmsCost) {
		this.mmsCost = mmsCost;
	}

	public int getInternetCalculationUnit() {
		return internetCalculationUnit;
	}

	public void setInternetCalculationUnit(int internetCalculationUnit) {
		this.internetCalculationUnit = internetCalculationUnit;
	}

	/** OPTIMIZE!! */
	public boolean isCallForFree(String number) {
		return isFree(number, freeCallNetworks);
	}
	
	/** OPTIMIZE!! */
	public boolean isSmsForFree(String number) {
		return isFree(number, freeSmsNetworks);
	}
	
	private boolean isFree(String number, String[] array) {
		if(array == null)
			return false;
		
		PhoneNumber numberProto = null;
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		try {
		  numberProto = phoneUtil.parse(number, "HR");
		} catch (NumberParseException e) {
		  System.err.println("NumberParseException was thrown: " + e.toString());
		}
		
//		Log.d("DEBUG", numberProto.toString());
		boolean isValid = phoneUtil.isValidNumber(numberProto);
		
		if(!isValid) {
//			Log.d("INVALID", numberProto.toString());
			return false;
		}
		
		number = String.valueOf(numberProto.getCountryCode()) + String.valueOf(numberProto.getNationalNumber());
		for (int i = 0; i < array.length; i++) {
			String operator = DataManager.getNumberOperatorMap().get(number);
			if(operator == null)
				continue;
			if(operator.equals(array[i]))
				return true;
		}
		
		return false;
	}
}
