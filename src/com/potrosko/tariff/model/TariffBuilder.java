package com.potrosko.tariff.model;

public class TariffBuilder {
	public final String OPERATOR;
	public final String TYPE;
	public final String NAME;
	
	// ----- FEES -------------

	public double monthlyCost;

	/** Call cost in format cost/minute. */
	public double callCost;
	
	/** Cost for starting a phone call. */
	public double callStartCost;
	
	/** How many seconds will pass before a new call cost will tick */
	public int callCalculationUnit;
	
	public double smsCost;
	public double mmsCost;
	
	public double internetTrafficCost;
	
	/** Internet traffic cost per number of bytes here */
	public int internetCalculationUnit;
	
	// ------ OTHER COSTS ---------
	
	public double activationCost;
	
	// ------ FREE items ----------
	
	public String[] freeCallNetworks;
	public String[] freeSmsNetworks;
	
	/**
	 * Amount of free calling minutes
	 */
	public double freeCalls;
	
	/**
	 * Amount of free sms
	 */
	public int freeSms;
	
	/**
	 * Amount of free internet traffic
	 */
	public double freeInternetTraffic;
	
	/**
	 * Discount percentage
	 */
	public double discount;
	
	public TariffBuilder(String OPERATOR, String TYPE, String NAME) {
		this.OPERATOR = OPERATOR;
		this.TYPE = TYPE;
		this.NAME = NAME;
	};
	
	public Tariff build() {
		return new Tariff(this);
	}

	public TariffBuilder setMonthlyCost(double monthlyCost) {
		this.monthlyCost = monthlyCost;
		return this;
	}

	public TariffBuilder setCallCost(double callCost) {
		this.callCost = callCost;
		return this;
	}

	public TariffBuilder setCallStartCost(double callStartCost) {
		this.callStartCost = callStartCost;
		return this;
	}

	public TariffBuilder setCallCalculationUnit(int callCalculationUnit) {
		this.callCalculationUnit = callCalculationUnit;
		return this;
	}

	public TariffBuilder setSmsCost(double smsCost) {
		this.smsCost = smsCost;
		return this;
	}

	public TariffBuilder setMmsCost(double mmsCost) {
		this.mmsCost = mmsCost;
		return this;
	}

	public TariffBuilder setInternetTrafficCost(double internetTrafficCost) {
		this.internetTrafficCost = internetTrafficCost;
		return this;
	}

	public TariffBuilder setInternetCalculationUnit(int internetCalculationUnit) {
		this.internetCalculationUnit = internetCalculationUnit;
		return this;
	}
	
	public TariffBuilder setActivationCost(double activationCost) {
		this.activationCost = activationCost;
		return this;
	}

	public TariffBuilder setFreeCallNetworks(String[] freeCallNetworks) {
		this.freeCallNetworks = freeCallNetworks;
		return this;
	}

	public TariffBuilder setFreeSmsNetworks(String[] freeSmsNetworks) {
		this.freeSmsNetworks = freeSmsNetworks;
		return this;
	}

	public TariffBuilder setFreeCalls(double freeCalls) {
		this.freeCalls = freeCalls;
		return this;
	}

	public TariffBuilder setFreeSms(int freeSms) {
		this.freeSms = freeSms;
		return this;
	}

	public TariffBuilder setFreeInternetTraffic(double freeInternetTraffic) {
		this.freeInternetTraffic = freeInternetTraffic;
		return this;
	}

	public TariffBuilder setDiscount(double discount) {
		this.discount = discount;
		return this;
	}
}
