package com.potrosko.tariff.calculator;

import java.util.ArrayList;

import com.potrosko.log.model.PhonecallLog;
import com.potrosko.log.model.SmsLog;

public interface ITariffCalculator {
	public double calculateSmsCost(ArrayList<SmsLog> smsLogs);
	public double calculateCallCost(ArrayList<PhonecallLog> callLogs);
	public double calculateInternetTrafficCost(long bytes);
}
