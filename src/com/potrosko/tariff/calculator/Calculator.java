package com.potrosko.tariff.calculator;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import android.util.Log;

import com.potrosko.controller.DataManager;
import com.potrosko.controller.LogFilter;
import com.potrosko.log.manager.InternetTrafficManager;
import com.potrosko.log.model.AbstractLog;
import com.potrosko.log.model.PhonecallLog;
import com.potrosko.log.model.SmsLog;
import com.potrosko.tariff.model.Tariff;

public class Calculator {
	
	private static int tmpFreeSms;
	private static double tmpFreeCalls;
	
	public static final int MONTHLY_COST = 0;
	public static final int INTERNET_COST = 1;
	public static final int CALL_COST = 2;
	public static final int SMS_COST = 3;
	
	public static final double RADIO_FREQUENCY_COST = 5;
	
	private Calculator() {}
	
	public static double[] calculateCosts(Tariff tariff) {
		int month = Calendar.getInstance().get(Calendar.MONTH);
		ArrayList<AbstractLog> callLogs = LogFilter.filterByMonth(DataManager.getCallLogs(), month);
		ArrayList<AbstractLog> smsLogs = LogFilter.filterByMonth(DataManager.getSmsLogs(), month);
		return calculateCosts(tariff, callLogs, smsLogs);
	}
	
	public static double[] calculateCosts(Tariff tariff, ArrayList<AbstractLog> callLogs, ArrayList<AbstractLog> smsLogs) {		
		double[] results = new double[5];
		results[MONTHLY_COST] = calculateMonthlyCost(tariff);
		results[INTERNET_COST] = calculateInternetTrafficCost(tariff);
		results[CALL_COST] = calculateCallCost(callLogs, tariff);
		results[SMS_COST] = calculateSmsCost(smsLogs, tariff);	
		for (int i = 0; i < results.length - 1; i++) {
			results[results.length - 1] += results[i];
		}
		
		results[results.length - 1] += RADIO_FREQUENCY_COST;
		return results;
	}
	
	public static double calculateMonthlyCost(Tariff tariff) {
		double totalCost = 0;
		totalCost = tariff.getMonthlyCost() * (1 - tariff.getDiscount() / 100);
		return totalCost;
	}
	
	public static double calculateInternetTrafficCost(Tariff tariff) {
		long bytes = Math.abs(InternetTrafficManager.getTotalBytes() - InternetTrafficManager.getTotalStartBytes());
		long monthlyBytes = DataManager.getMonthlyInternetTraffic();
		long totalBytes = bytes + monthlyBytes;
		return calculateInternetTrafficCost(totalBytes, tariff);
	}

	public static double calculateInternetTrafficCost(long bytes, Tariff tariff) {
		
		double mb = DataManager.toMegabytes(bytes);
		
		if(mb > tariff.getFreeInternetTraffic()) {
			mb = mb - tariff.getFreeInternetTraffic();
		} else {
			return 0;
		}
					
		return tariff.getInternetTrafficCost() * (mb / (tariff.getInternetCalculationUnit() / 1000.0) + 1);
	}

	public static double calculateSmsCost(ArrayList<AbstractLog> smsLogs, Tariff tariff) {
		double totalCost = 0;
		tmpFreeSms = tariff.getFreeSms();
		
		for (int i = 0; i < smsLogs.size(); i++) {
			SmsLog log = (SmsLog) smsLogs.get(i);
			totalCost += calculateSmsCost(log, tariff);
		}		
		DataManager.storeFreeSmsData(tmpFreeSms);
		
		return totalCost;
	}
	
	public static double calculateSmsCost(SmsLog log, Tariff tariff) {
		double cost = 0;

		//check if the network of the number sms is sent to is free for messaging
		if(tariff.isSmsForFree(log.getNumberSent())) 
			return 0;
		
		if(tmpFreeSms > 0) {
			tmpFreeSms--;
		} else {
			cost += tariff.getSmsCost();
		}
		
		return cost;
	}

	public static double calculateCallCost(ArrayList<AbstractLog> callLogs, Tariff tariff) {
		double totalCost = 0;
		tmpFreeCalls = tariff.getFreeCalls();
		
		SimpleDateFormat s = new SimpleDateFormat("MMM dd yyyy hh:mm:ss:SSS a", Locale.US);
		
		double cost;
		for (int i = 0; i < callLogs.size(); i++) {
			PhonecallLog log = (PhonecallLog) callLogs.get(i);
			
//			Log.d("CALC", s.format(log.getDate())); 
//			Log.d("CALC", log.getNumberDialed() + " " + String.valueOf(log.getDuration())); 
			
			cost = calculateCallCost(log, tariff);
			
			HashMap<String, String> map = DataManager.getNumberOperatorMap();
			if(cost != 0)
				Log.d("CALC", log.getNumberDialed() + " " + String.valueOf(log.getDuration()) + " Cost: " +
					String.valueOf(cost) + " " + s.format(log.getDate()) + " " + 
						map.get(log.getNumberDialed()) +
						" Besplatni poziv: " + 
						tariff.isCallForFree(log.getNumberDialed())); 
			
			totalCost += cost;
		}		
		DataManager.storeFreeCallData(tmpFreeCalls);
		
		return totalCost;
	}
	
	public static double calculateCallCost(PhonecallLog log, Tariff tariff) {
		double cost = 0;
		
		if(log.getDuration() == 0)
			return 0;
		
		cost += tariff.getCallStartCost();
		
		//check if the network of the dialed number is free to call
		if(tariff.isCallForFree(log.getNumberDialed())) 
			return cost;
		
		//duration of the call in minutes
		double minutes = log.getDuration() * 1.0 / 60;		
		
		//check if there are any free call minutes
		if(minutes > tmpFreeCalls) {
			
			//number of times the call cost will tick
			int paidSeconds = (int) (minutes - tmpFreeCalls) * 60;
			int paidUnits = paidSeconds / tariff.getCallCalculationUnit() + 1;
			
			cost += paidUnits * tariff.getCallCost();
		} else if(minutes <= tmpFreeCalls) {
			tmpFreeCalls -= minutes;
		}
		
		return cost;
	}
}
