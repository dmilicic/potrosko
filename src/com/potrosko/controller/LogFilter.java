package com.potrosko.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.util.Log;

import com.potrosko.log.model.AbstractLog;

public class LogFilter {

	public static ArrayList<AbstractLog> filterByMonth(ArrayList<AbstractLog> arr, int month) {
		
		ArrayList<AbstractLog> deleteList = new ArrayList<AbstractLog>();		
				
		Calendar cal = Calendar.getInstance();
		for (int i = 0; i < arr.size(); i++) {
			Date date = new Date(arr.get(i).getDate());
			cal.setTime(date);	
			if(cal.get(Calendar.MONTH) != month) {
				deleteList.add(arr.get(i));
			}
		}
		
		for (int i = 0; i < deleteList.size(); i++) {
			arr.remove(deleteList.get(i));
		}
		
		Log.d("KOLICINA", String.valueOf(arr.size()));
		
		return arr;
	}
}
