package com.potrosko.controller.asynctasks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.potrosko.MainActivity;
import com.potrosko.controller.DataManager;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Class responsible for requesting number-operator information from the server
 *
 */
public class NumberOperatorTask extends AsyncTask<String, Void, String> {
	
	private final String TAG = "ASYNCTASK";
	
	private String postData(String... numbers) {
	    // Create a new HttpClient and Post Header
	    HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httppost = new HttpPost(DataManager.NUMBER_OPERATOR_URL);

	    HttpResponse response = null;
	    
	    try {
	        // Add your data
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(numbers.length);
	        for (int i = 0; i < numbers.length; i++) {
	        	nameValuePairs.add(new BasicNameValuePair("number" + String.valueOf(i), numbers[i]));
	        }
	        
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));	     
	        
	        Log.d(TAG, "Starting download..." + numbers.length + " items");
	     

	        // Execute HTTP Post Request
	        response = httpclient.execute(httppost);	    
	        return EntityUtils.toString(response.getEntity());
	        
	    } catch (ClientProtocolException e) {
	    	Log.e("ERR", e.getMessage());
	    } catch (IOException e) {
	    	Log.e("ERR", e.getMessage());
	    }
	    
	    Log.e("ERR", DataManager.SERVER_ERROR);
	    return DataManager.SERVER_ERROR;
	} 
	
	@Override
	protected String doInBackground(String... numbers) {		
		//get the json file containing number-operator information
		String result = postData(numbers);
		
		if(result.equals(DataManager.SERVER_ERROR))
			return null;
		
		return result;
	}
	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		
		if (result == null) {
			Log.d(TAG, "Download done but no result :(");
			return;
		}
		
		//load number-operator information to memory
		DataManager.setupNumberOperatorMap(result);		
		
		//store data to a local file for future use
		DataManager.storeData(result, DataManager.FILE_NUMBERS);	
		
		MainActivity.DOWNLOAD_DONE = true;
		
		Log.d(TAG, "Download done!");
	}
}
