package com.potrosko.controller.asynctasks;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.potrosko.controller.DataManager;

import android.os.AsyncTask;
import android.util.Log;

public class TariffDownloadTask extends AsyncTask<Void, Void, String> {
	
	private String getTariffData() {
	    // Create a new HttpClient and Post Header
	    HttpClient httpclient = new DefaultHttpClient();
	    HttpGet httpget = new HttpGet(DataManager.TARIFF_DOWNLOAD_URL);
	    HttpResponse response = null;
	    
	    try {	      
	        
	    	Log.d("ASYNCTASK", "Starting tariff download...");
	     
	        // Execute HTTP Get Request
	        response = httpclient.execute(httpget);	
	        return EntityUtils.toString(response.getEntity());
	        
	    } catch (ClientProtocolException e) {
	    } catch (IOException e) {
	    }
	    
		return DataManager.SERVER_ERROR;  
	} 

	@Override
	protected String doInBackground(Void... arg0) {
		String result = getTariffData();
		DataManager.storeData(result, DataManager.FILE_TARIFFS);
		return result;
	}
}
