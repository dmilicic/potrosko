package com.potrosko.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import com.potrosko.log.manager.InternetTrafficManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

/**
 * Broadcast receiver class responsible for receiving and handling relevant broadcast intents
 */
public class AppBroadcastReceiver extends BroadcastReceiver {
	
	private static final String TIMESTAP_PATTERN = "MMM dd yyyy";
	private static SimpleDateFormat sDateFormat = new SimpleDateFormat(TIMESTAP_PATTERN, Locale.US);
	
	private void saveInternetTrafficInfoShutdown(Context context) {	
		Calendar cal = Calendar.getInstance();
		String today = String.valueOf(sDateFormat.format(cal.getTime()));
		String month = String.valueOf(cal.get(Calendar.MONTH));
		
		SharedPreferences prefs = context.getSharedPreferences(DataManager.PREFS_NAME, 0);
		SharedPreferences.Editor editor = prefs.edit();
						
		//total traffic of today's month
		Long totalTraffic = prefs.getLong(month, 0);
		
		//total traffic for today up to this point in time
		Long traffic = prefs.getLong(today, 0);
		
		//currently amounted traffic 
		Long sessionTraffic = Math.abs(InternetTrafficManager.getTotalBytes() - InternetTrafficManager.getTotalStartBytes());
		
		//total traffic to be added to today's traffic
		Long resultTraffic = traffic + sessionTraffic;
		
		//update internet usage info for today
		editor.putLong(today, resultTraffic);

		//update internet usage for the whole month
		editor.putLong(month, totalTraffic + sessionTraffic);
		
		//resets the number of starting bytes for the next boot
		editor.putLong(DataManager.PREFS_START_RX_BYTES, 0);
		editor.putLong(DataManager.PREFS_START_TX_BYTES, 0);
		
		editor.commit();
	}
	
	private void saveInternetTrafficInfoDateChanged(Context context) {
		Calendar cal = Calendar.getInstance();
		
		//get yesterday's date
		cal.add(Calendar.DATE, -1);
		String yesterday = String.valueOf(sDateFormat.format(cal.getTime()));
		String month = String.valueOf(cal.get(Calendar.MONTH));
		
		
		SharedPreferences prefs = context.getSharedPreferences(DataManager.PREFS_NAME, 0);
		SharedPreferences.Editor editor = prefs.edit();
				
		//total traffic of yesterday's month
		Long totalTraffic = prefs.getLong(month, 0);
		
		//total traffic of yesterday up to this point in time
		Long traffic = prefs.getLong(yesterday, 0);
		
		//currently amounted traffic 
		Long sessionTraffic = Math.abs(InternetTrafficManager.getTotalBytes() - InternetTrafficManager.getTotalStartBytes());
		
		//total traffic to be added to yesterday's traffic
		Long resultTraffic = traffic + sessionTraffic;
		
		//update internet usage info for yesterday		
		editor.putLong(yesterday, resultTraffic);
		
		//update internet usage info for the whole month
		editor.putLong(month, totalTraffic + sessionTraffic);
		
		//resets the number of starting bytes for the next day
		editor.putLong(DataManager.PREFS_START_RX_BYTES, InternetTrafficManager.getRxBytes());
		editor.putLong(DataManager.PREFS_START_TX_BYTES, InternetTrafficManager.getTxBytes());
		
		editor.commit();		
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		
		DataManager.context = context;
		
		if(intent.getAction().equals(Intent.ACTION_SHUTDOWN))
			saveInternetTrafficInfoShutdown(context);
		else if (intent.getAction().equals(Intent.ACTION_DATE_CHANGED)) {
			saveInternetTrafficInfoDateChanged(context);
		}
	}
}
