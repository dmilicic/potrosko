package com.potrosko.controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.NoSuchElementException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;

import com.google.i18n.phonenumbers.NumberParseException;
import com.potrosko.controller.asynctasks.NumberOperatorTask;
import com.potrosko.controller.asynctasks.TariffDownloadTask;
import com.potrosko.log.manager.AbstractLogManager;
import com.potrosko.log.manager.InternetTrafficManager;
import com.potrosko.log.manager.PhonecallLogManager;
import com.potrosko.log.manager.SmsLogManager;
import com.potrosko.log.model.AbstractLog;
import com.potrosko.log.model.PhonecallLog;
import com.potrosko.log.model.SmsLog;
import com.potrosko.tariff.model.Tariff;

public class DataManager {
	
	//shared preferences data
	public static final String PREFS_NAME = "potrosko-prefs";
	public static final String PREFS_TARIFF_NAME = "TARIFF_NAME";
	public static final String PREFS_START_RX_BYTES = "startRX";
	public static final String PREFS_START_TX_BYTES = "startTX";
	public static final String PREFS_FIRST_RUN = "firstRun";
	public static final String PREFS_FREE_SMS = "freeSms";
	public static final String PREFS_FREE_CALLS = "freeCalls";
	
	//app file data
	public static final String FILE_ACTIVE_TARIFF = "active-tariff";
	public static final String FILE_TARIFFS = "tariffs.txt";
	public static final String FILE_NUMBERS = "phone-numbers.txt";
	
	//server configuration data
	public static final String SERVER = "http://ec2-54-200-207-55.us-west-2.compute.amazonaws.com";
	public static final String NUMBER_OPERATOR_URL = SERVER + "/numbers";
	public static final String TARIFF_DOWNLOAD_URL = SERVER + "/tariffs";
	public static final String SERVER_ERROR = "Error";
	
	private static final DecimalFormat decimalFormat = new DecimalFormat("#.##");
	
	public static ArrayList<Object> tariffs;	
	
	/** The context of the application, used for opening/closing app files */
	public static Context context;
	
	/**
	 * Reads and returns the string from a given file
	 */
    private static String readFromFile(InputStream in) {

        String ret = "";

        try {
            InputStream inputStream = in;

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
	
	/**
	 * Downloads the new tariff information from the server database
	 */
	public static TariffDownloadTask downloadTariffs() {
		TariffDownloadTask task = new TariffDownloadTask();
		task.execute();
		return task;
	}
    
	/**
	 * Downloads number-operator information from the server for a given array of phone numbers
	 */
	public static NumberOperatorTask updateNumberOperatorMap(String[] numbers) {
		NumberOperatorTask task = new NumberOperatorTask();
		task.execute(numbers);
		return task;
	}
    
	/**
	 * Loads information about number and its mobile operator from a given json file to app memory
	 */
	public static void setupNumberOperatorMap(InputStream in) {
        String jsonData = readFromFile(in);                       
        setupNumberOperatorMap(jsonData);
	}
	
	/**
	 * Loads information about number and its mobile operator from a given json data to app memory
	 */
	public static void setupNumberOperatorMap(String jsonData) {
   				
        try {
           JSONArray jArray = new JSONArray(jsonData);                             
           
           for (int i = 0; i < jArray.length(); i++) {
                   JSONObject jsonObject = jArray.getJSONObject(i);
                   NumberOperator.fromJsonToMap(jsonObject);
           }                          
        } catch (JSONException e) {
           System.err.println(jsonData);
           e.printStackTrace();
        }        
	}
	
	/** Gets all the sms logs from the phone */
	public static ArrayList<AbstractLog> getSmsLogs() {
		SmsLogManager sManager = new SmsLogManager(context.getContentResolver());
		return sManager.getLogs();
	}
	
	/** Gets the available sms logs for the given month */
	public static ArrayList<AbstractLog> getSmsLogs(int month) {
		return LogFilter.filterByMonth(DataManager.getSmsLogs(), month);
	}
	
	/** Gets all the call logs available from the phone */
	public static ArrayList<AbstractLog> getCallLogs() {
		PhonecallLogManager pManager = new PhonecallLogManager(context.getContentResolver());
		return pManager.getLogs();
	}
	
	/** Gets the available call logs for the given month */
	public static ArrayList<AbstractLog> getCallLogs(int month) {
		return LogFilter.filterByMonth(DataManager.getCallLogs(), month);
	}
	
	/**
	 * Gets the amount of internet traffic in MB for a given month (current month)
	 * @return
	 */
	public static long getMonthlyInternetTraffic() {
		InternetTrafficManager iManager = InternetTrafficManager.getInstance();
		return iManager.getMonthlyTraffic();
	}
	
	public static HashMap<String, String> getNumberOperatorMap() {
		
		if(NumberOperator.numberToOperatorMap == null) {
			NumberOperator.numberToOperatorMap = new HashMap<String, String>();
		}
		
		return NumberOperator.numberToOperatorMap;
	}
	
	/**
	 * Gets all the phone numbers stored in users phone (from contacts)
	 * @return String array of all the phone numbers from contacts
	 */
	public static String[] getAllContactNumbers() {
		ArrayList<String> alContacts = new ArrayList<String>();		
		
		Cursor cursor = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
		if(cursor.moveToFirst())
		{
		    
		    do
		    {
		        String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));

		        if(Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
		        {
		            Cursor pCur = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
		            										 null,
		            										 ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
		            										 new String[]{ id }, 
		            										 null);
		            while (pCur.moveToNext()) 
		            {
		                String contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));			                		                
		        		try {
							alContacts.add(AbstractLogManager.formalizePhoneNumber(contactNumber));
						} catch (NumberParseException e) {
							System.err.println(e.toString());
							System.err.println(contactNumber);
						}		                
		                break;
		            }
		            pCur.close();
		        }

		    } while (cursor.moveToNext());
		    cursor.close();
		}
		
		String[] numbers = new String[alContacts.size()];
		for (int i = 0; i < alContacts.size(); i++) {
			numbers[i] = alContacts.get(i);
		}
		
		return numbers;
	}
	
	/**
	 * Gets all the phone numbers that were called from this phone
	 * @return String array of all the phone numbers that were called
	 */
	public static String[] getCalledNumbers() {
		ArrayList<AbstractLog> calledLogs = new ArrayList<AbstractLog>();		
		
		PhonecallLogManager manager = new PhonecallLogManager(context.getContentResolver());
		calledLogs = manager.getLogs();
		
		
		String[] numbers = new String[calledLogs.size()];
		for (int i = 0; i < calledLogs.size(); i++) {
			numbers[i] = ((PhonecallLog) calledLogs.get(i)).getNumberDialed();
		}
		
		return numbers;
	}
	
	/**
	 * Gets all the phone numbers to which the user had sent an sms
	 * @return String array of all the phone numbers that were called
	 */
	public static String[] getMessagedNumbers() {
		ArrayList<AbstractLog> smsLogs = new ArrayList<AbstractLog>();		
		
		SmsLogManager manager = new SmsLogManager(context.getContentResolver());
		smsLogs = manager.getLogs();
		
		
		String[] numbers = new String[smsLogs.size()];
		for (int i = 0; i < smsLogs.size(); i++) {
			numbers[i] = ((SmsLog) smsLogs.get(i)).getNumberSent();
		}
		
		return numbers;
	}
	
	
	/**
	 * Gets ALL the phone numbers stored in users phone
	 * @return String array of all the phone numbers
	 */
	public static String[] getAllNumbers() {
		
		long startTime = System.nanoTime();		
		
		String[] contacts = getAllContactNumbers();
		String[] called = getCalledNumbers();
		String[] messaged = getMessagedNumbers();
		
		HashSet<String> numSet = new HashSet<String>();	
		for (int i = 0; i < contacts.length; i++) {
			numSet.add(contacts[i]);
		}
		
		for (int i = 0; i < called.length; i++) {
			numSet.add(called[i]);
		}
		
		for (int i = 0; i < messaged.length; i++) {
			numSet.add(messaged[i]);
		}
		
		String[] result = new String[numSet.size()];
		int idx = 0;
		for(String s : numSet){
			result[idx++] = s;
		}
		
		//execution time
		long endTime = System.nanoTime();
		long duration = endTime - startTime;
		Log.d("GET ALL NUMBERS", String.valueOf(duration / 1000000) + " miliseconds");
		
		return result;
	}
	
	/** Gets all the tariffs that are stored in the app file storage or app asset storage */
	public static ArrayList<Object> getTariffs() {
		ArrayList<Object> result = null;
		 try {
			result = parseTariffData(context.openFileInput(FILE_TARIFFS));
		} catch (Exception e) {
			try {
				result = parseTariffData(context.getAssets().open(FILE_TARIFFS, 0));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} 
	 
		 return result;
	}
	
	/** Gets all the tariffs from a given operator that are stored in the app file storage */
	public static ArrayList<Object> getTariffs(String operator) {
		ArrayList<Object> result = null;
		try {
			result = parseTariffData(context.openFileInput(FILE_TARIFFS), operator);
		} catch (Exception e) {
			try {
				result = parseTariffData(context.getAssets().open(FILE_TARIFFS), operator);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		return result;
	}
	
    /**
     * Gets the currently active tariff. Note: Not efficient, fix for O(log n) or O(1)
     */
    public static Tariff getCurrentTariff() 
    		throws NoSuchElementException {
    	
    	SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
    	String tariffName = prefs.getString(PREFS_TARIFF_NAME, null);
    	
    	Tariff current = null;   	
	
    	//there is no active tariff in app memory, load tariff from file
		try {
			current = parseTariff(context.openFileInput(FILE_ACTIVE_TARIFF));
			System.out.println("TU SAM");
			return current;
		} catch (Exception ef) {
			ef.printStackTrace();
			
	    	//load the tariff from the list of all tariffs
	    	ArrayList<Object> tariffs = null;
	    	try {
				tariffs = parseTariffData(context.openFileInput(FILE_TARIFFS));
			} catch (Exception e) {
				e.printStackTrace();
			}
	    	
	    	if(tariffName == null || tariffs == null)
	    		throw new NoSuchElementException();    	
	    	
	    	//find the active tariff in the list of all tariffs
	    	for (int i = 0; i < tariffs.size(); i++) {
				Tariff t = (Tariff) tariffs.get(i);
				if(t.getNAME().equals(tariffName)) {
					current = t;
					break;
				}
			}				
		}
   

    	        	
//    	Toast.makeText(context, current.getNAME(), Toast.LENGTH_SHORT).show();
    	return current;
    }
	
	/**
	 * Parses json format file of tariffs into an array of tariffs in app memory
	 */
	private static ArrayList<Object> parseTariffData(InputStream in) throws JSONException {
        ArrayList<Object> array = new ArrayList<Object>();
        String jsonData = readFromFile(in);               

        JSONArray jArray = new JSONArray(jsonData);                             
       
        for (int i = 0; i < jArray.length(); i++) {
        	JSONObject jsonObject = jArray.getJSONObject(i);
        	array.add(Tariff.fromJson(jsonObject));
        }               
        
        tariffs = array;        
        return array;
	}
	
	/**
	 * Parses json format file of tariffs into an array of tariffs for a specific operator to app memory
	 */
	private static ArrayList<Object> parseTariffData(InputStream in, String Operator) throws JSONException {
        ArrayList<Object> array = new ArrayList<Object>();
        String jsonData = readFromFile(in);               
   
        JSONArray jArray = new JSONArray(jsonData);                             
       
        for (int i = 0; i < jArray.length(); i++) {
               JSONObject jsonObject = jArray.getJSONObject(i);
               Tariff tariff = Tariff.fromJson(jsonObject);
               if(tariff.getOPERATOR().equals(Operator))
            	   array.add(Tariff.fromJson(jsonObject));
        }               
       
        return array;
	}
	
	/**
	 * Parses json  format file of the active tariff
	 */
	private static Tariff parseTariff(InputStream in) throws JSONException {
        String jsonData = readFromFile(in);   
        Tariff tariff = Tariff.fromJson(new JSONObject(jsonData));                              
        return tariff;
	}
    
    /**
     * Stores given string data to a given file
     */
    public static void storeData(String data, String filename) {
    	storeData(context, data, filename);
    }
	
    /**
     * Stores given string data to a given file
     */
    public static void storeData(Context context, String data, String filename) {
    	FileOutputStream fos;
		try {
			fos = context.openFileOutput(filename, Context.MODE_PRIVATE);			
	    	fos.write(data.getBytes());
	    	fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    /** Stores the amount of free sms left */
    public static void storeFreeSmsData(int value) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putInt(PREFS_FREE_SMS, value);
		editor.commit();
    }
    
    
    /** Gets the amount of free sms left */
    public static int getFreeSmsData() {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);	
		return prefs.getInt(PREFS_FREE_SMS, 0);
    }
    
    
    /** Stores the amount of free call minutes left */
    public static void storeFreeCallData(double value) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putFloat(PREFS_FREE_CALLS, (float)value);
		editor.commit();
    }
    
    
    /** Gets the amount of free call minutes left */
    public static double getFreeCallData() {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);	
		return prefs.getFloat(PREFS_FREE_CALLS, 0);
    }
    
    /** Adds currency to users cost info */
    public static String addCurrency(String cost) {
    	return cost + "kn";
    }
    
    /** Puts the cost value to a readable format with added currency */
    public static String toCost(double cost) {
    	return addCurrency(decimalFormat.format(cost));
    }
        
    public static double toMegabytes(long bytes) {
    	return InternetTrafficManager.toMegabytes(bytes);
    }
    
    public static String formatByteUsage(long used, double totalMB) {
    	return formatBytes(used) + "/" + format(totalMB) + " MB";
    }
    
    public static String formatBytes(long bytes) {
    	return decimalFormat.format(DataManager.toMegabytes(bytes));
    }
    
    /** Formats decimal numbers */
    public static String format(double val) {
    	return decimalFormat.format(val);
    }

    private static class NumberOperator {

    	private static final String NUMBER = "Number";
    	private static final String OPERATOR = "Operator";
    	
    	/**
    	 * A hash map that links a number to its mobile operator
    	 */
    	private static HashMap<String, String> numberToOperatorMap;
    	
    	private static void fromJsonToMap(JSONObject object) {
    		
    		if(numberToOperatorMap == null) {
    			numberToOperatorMap = new HashMap<String, String>();
    		}                        
    		
    		String operator = null;
    		String number = null;                                                                                                                                                      
    		
    		try {
    			number = object.getString(NUMBER);
    			operator = object.getString(OPERATOR);
    		} catch (JSONException e) {
    			e.printStackTrace();
    		}
    		
//    		System.out.println(number + " " + operator);
    		
    		numberToOperatorMap.put(number, operator);
    	}     
    }
}
