package com.potrosko;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import android.annotation.TargetApi;
import android.app.ListActivity;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;

import com.potrosko.controller.DataManager;
import com.potrosko.log.model.AbstractLog;
import com.potrosko.log.model.PhonecallLog;
import com.potrosko.log.model.SmsLog;
import com.potrosko.tariff.calculator.Calculator;
import com.potrosko.tariff.model.Tariff;

public class EventLogActivity extends ListActivity {
	
	private int month;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Show the Up button in the action bar.
		setupActionBar();
		init();
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.event_log, menu);
		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void init() {		
		
		month = Calendar.getInstance().get(Calendar.MONTH);
		
        // Create a progress bar to display while the list loads
        ProgressBar progressBar = new ProgressBar(this);
        progressBar.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT));     
        progressBar.setIndeterminate(true);
        getListView().setEmptyView(progressBar);

        // Must add the progress bar to the root of the layout
        ViewGroup root = (ViewGroup) findViewById(android.R.id.content);
        root.addView(progressBar);    
        
        EventLogTask task = new EventLogTask();
        task.execute(month);
	}	
	
	/** Background task class that is responsible to give the event log for the given month. */
	private class EventLogTask extends AsyncTask<Integer, Void, List<HashMap<String, Object>>> {
		
		/** Class that represents a single list item of the event log */
		private class EventLogItem {
			
			public static final String TYPE = "type";
			public static final String NUMBER_SENT = "numberSent";
			public static final String COST = "cost";
			
			private final String type;
			private final String numberSent;
			private final double cost;
			
			public EventLogItem(String type, String numberSent, double cost) {
				this.type = type;
				this.numberSent = numberSent;
				this.cost = cost;
			}
			
			public String getType() {
				return type;
			}
			
			public String getNumberSent() {
				return numberSent;
			}
			
			public double getCost() {
				return cost;
			}
		}
		
		private void setupListAdapter(List<HashMap<String, Object>> result) {
	        
	        // parameters for the list adapter
	        String[] from = new String[] { EventLogItem.TYPE, EventLogItem.NUMBER_SENT, EventLogItem.COST };
	        int[] to = new int[] {R.id.eventLogType, R.id.eventLogName, R.id.eventLogCost };	  
	        
	        // setup the list adapter
			SimpleAdapter adapter = null;
			adapter = new SimpleAdapter(EventLogActivity.this, result, 
					R.layout.list_eventlog_item, from, to);
			setListAdapter(adapter);
		}
		
		/** Method for merging sms and call logs based on date to one array. */
		private ArrayList<AbstractLog> mergeLogs(ArrayList<AbstractLog> calls, ArrayList<AbstractLog> sms) {
			ArrayList<AbstractLog> merged = new ArrayList<AbstractLog>();
			
			// merge these two arrays
			int idx1 = 0, idx2 = 0;
			while(idx1 < calls.size() && idx2 < sms.size()) {
				
				if(idx1 == calls.size()) {
					merged.add(sms.get(idx2));
					idx2++;
					continue;
				}
				
				if(idx2 == sms.size()) {
					merged.add(calls.get(idx1));
					idx1++;
					continue;
				}
				
				if( calls.get(idx1).getDate() > sms.get(idx2).getDate() ) {
					merged.add(calls.get(idx1));
					idx1++;
				} else {
					merged.add(sms.get(idx2));
					idx2++;
				}
				
			}
			
			return merged;
		}

		@Override
		protected List<HashMap<String, Object>> doInBackground(Integer... params) {
	        int month = params[0];
	        
	        // list needed by the list view adapter
	        List<HashMap<String, Object>> fillMaps = new ArrayList<HashMap<String, Object>>();
	        
	        ArrayList<AbstractLog> calls = DataManager.getCallLogs(month);
	        ArrayList<AbstractLog> sms = DataManager.getSmsLogs(month);
	        ArrayList<AbstractLog> logs = mergeLogs(calls, sms);
	        
	        Tariff tariff = DataManager.getCurrentTariff();
	        
	        // calculate the cost for each log so list adapter can display the details
	        AbstractLog log;
	        String type = null, number = null;
	        double cost = 0;
	        HashMap<String, Object> map;
	        for (int i = 0; i < logs.size(); i++) {
				 log = logs.get(i);
				
				 // check what class is this log
				if (log instanceof PhonecallLog) {
					type = "Poziv";
					number = ((PhonecallLog) log).getNumberDialed();
					cost = Calculator.calculateCallCost((PhonecallLog)log, tariff);
				} else if (log instanceof SmsLog) {
					type = "SMS";
					number = ((SmsLog) log).getNumberSent();
					cost = Calculator.calculateSmsCost((SmsLog)log, tariff);
				}		
				
				if (cost == 0)
					continue;

		        map = new HashMap<String, Object>();
		        map.put(EventLogItem.TYPE, type);
		        map.put(EventLogItem.NUMBER_SENT, number);
		        map.put(EventLogItem.COST, DataManager.addCurrency(String.valueOf(cost)));
		        
		        fillMaps.add(map);
			}
	        
	        
	        return fillMaps;
		}
		
		@Override
		protected void onPostExecute(List<HashMap<String, Object>> result) {
			super.onPostExecute(result);
			setupListAdapter(result);
		}
	}
}
