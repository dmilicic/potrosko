package com.potrosko;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.NoSuchElementException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.potrosko.controller.DataManager;
import com.potrosko.controller.LogFilter;
import com.potrosko.controller.asynctasks.NumberOperatorTask;
import com.potrosko.log.manager.InternetTrafficManager;
import com.potrosko.log.model.AbstractLog;
import com.potrosko.tariff.calculator.Calculator;
import com.potrosko.tariff.model.Tariff;

public class MainActivity extends Activity 
		implements Runnable {
	
	/** Flag that marks if new number operator information was downloaded */
	public static boolean DOWNLOAD_DONE = false;
	
	private static final int INTERNET_MONITOR_TIMER = 5000;

	private TextView txtView;
	private Handler mHandler;
	private Tariff tariff;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		init();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
	        case R.id.action_reset:
				Intent operators = new Intent(MainActivity.this, OperatorsActivity.class);
				startActivity(operators);
                break;
	        case R.id.action_recommend:
				Intent recommender = new Intent(MainActivity.this, RecommenderActivity.class);
				startActivity(recommender);
                break;
	        case R.id.action_eventlog:
				Intent eventlog = new Intent(MainActivity.this, EventLogActivity.class);
				startActivity(eventlog);
                break;
	        default:
                break;
        }

        return true;
	}
	
	@Override
	protected void onResume() {
		super.onResume();					
		try {
			//gets the active tariff of the user
			tariff = DataManager.getCurrentTariff();
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Intent operators = new Intent(MainActivity.this, OperatorsActivity.class);
			startActivity(operators);
		}
		
		try {			
			//setup up the number to operator map
			DataManager.setupNumberOperatorMap(openFileInput(DataManager.FILE_NUMBERS));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//stop if there is no tariff
		if(tariff == null)
			return;
		
		//setup tariff name
		txtView = (TextView) findViewById(R.id.tariffTitleTextView);
		txtView.setText(tariff.getNAME());		
		
		calculateCosts();
		
		//start monitoring network traffic
		mHandler = new Handler();
		mHandler.postDelayed(this, INTERNET_MONITOR_TIMER);
	}
	
	@Override
	protected void onPause() {
		super.onPause();	
		
		if(mHandler != null) 
			mHandler.removeCallbacks(this);
	}
	
	@Override
	protected void onStop() {
		super.onStop();		
		mHandler = null;
		txtView = null;
	}
	
	public void init() {			
		DataManager.context = this;	
		
		try {
			//gets the active tariff of the user
			tariff = DataManager.getCurrentTariff();
			setContentView(R.layout.activity_main);		
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			
			//we need to select a tariff
			Intent operators = new Intent(MainActivity.this, OperatorsActivity.class);
			startActivity(operators);
			finish();
		}
	}
	
	public void calculateCosts() {
		double[] costs = Calculator.calculateCosts(tariff);
		
		// -------------------- CALCULATE MONTHLY COSTS ----------------------- //
		
		txtView = (TextView) findViewById(R.id.monthlyCostValueView);
		txtView.setText(DataManager.toCost(costs[Calculator.MONTHLY_COST]));
		
		// -------------------- CALCULATE CALL COSTS ----------------------- //
		
		txtView = (TextView) findViewById(R.id.callCostValueView);
		txtView.setText(DataManager.toCost(costs[Calculator.CALL_COST]));
		
		txtView = (TextView) findViewById(R.id.callUsageValueView);
		txtView.setText(DataManager.format(DataManager.getFreeCallData()) + " min");
		
		// -------------------- CALCULATE SMS COSTS ----------------------- //
		
		txtView = (TextView) findViewById(R.id.smsCostValueView);
		txtView.setText(DataManager.toCost(costs[Calculator.SMS_COST]));
		
		txtView = (TextView) findViewById(R.id.smsUsageValueView);
		txtView.setText(String.valueOf(DataManager.getFreeSmsData()));
		
		// -------------------- CALCULATE INTERNET TRAFFIC COSTS ----------------------- //
		
		txtView = (TextView) findViewById(R.id.internetCostValueView);
		txtView.setText(DataManager.toCost(costs[Calculator.INTERNET_COST]));
		
		long bytes = Math.abs(InternetTrafficManager.getTotalBytes() - InternetTrafficManager.getTotalStartBytes());
		long monthlyBytes = DataManager.getMonthlyInternetTraffic();
		
		long totalBytes = bytes + monthlyBytes;
		txtView = (TextView) findViewById(R.id.internetUsageValueView);
		txtView.setText(DataManager.formatByteUsage(totalBytes, tariff.getFreeInternetTraffic()));
		
		// -------------------- CALCULATE RADIO FREQUENCY COST ----------------------- //
		
		
		txtView = (TextView) findViewById(R.id.radioFreqCostValueView);
		txtView.setText(DataManager.addCurrency(String.valueOf(Calculator.RADIO_FREQUENCY_COST)));
		
		// -------------------- TOTAL COST ----------------------- //
		
		txtView = (TextView) findViewById(R.id.totalCostValueView);
		txtView.setText(DataManager.toCost(costs[costs.length - 1]));
	}
	
//	public void calculateCosts() {
//				
////		int month = Calendar.JULY;
//		int month = Calendar.getInstance().get(Calendar.MONTH);
//
//		// -------------------- CALCULATE MONTHLY COSTS ----------------------- //
//		
//		double totalMonthlyCost = Calculator.calculateMonthlyCost(tariff);
//		txtView = (TextView) findViewById(R.id.monthlyCostValueView);
//		txtView.setText(DataManager.toCost(totalMonthlyCost));
//									
//		// -------------------- CALCULATE CALL COSTS ----------------------- //
//		
//		long startTime = System.nanoTime();
//		
//		double totalCallCosts = Calculator.calculateCallCost(LogFilter.filterByMonth(DataManager.getCallLogs(), month), tariff);	
//		txtView = (TextView) findViewById(R.id.callCostValueView);
//		txtView.setText(DataManager.toCost(totalCallCosts));
//		
//		txtView = (TextView) findViewById(R.id.callUsageValueView);
//		txtView.setText(DataManager.format(DataManager.getFreeCallData()) + " min");
//		
//		//execution time
//		long endTime = System.nanoTime();
//		long duration = endTime - startTime;
//		Log.d("EXECUTE_CALLS", String.valueOf(duration / 1000000) + " miliseconds");
//		startTime = System.nanoTime();
//		
//		// -------------------- CALCULATE SMS COSTS ----------------------- //
//		
//		ArrayList<AbstractLog> smsLogs = LogFilter.filterByMonth(DataManager.getSmsLogs(), month);
//		
//		Log.d("SMSER", "Number of sms to calc: " + String.valueOf(smsLogs.size()));
//		
//		double totalSmsCosts = Calculator.calculateSmsCost(smsLogs, tariff);
//		
//		txtView = (TextView) findViewById(R.id.smsCostValueView);
//		txtView.setText(DataManager.toCost(totalSmsCosts));
//		
//		txtView = (TextView) findViewById(R.id.smsUsageValueView);
//		txtView.setText(String.valueOf(DataManager.getFreeSmsData()));
//		
//		//execution time
//		endTime = System.nanoTime();
//		duration = endTime - startTime;
//		Log.d("EXECUTE_SMS", String.valueOf(duration / 1000000) + " miliseconds");
//		
//		// -------------------- CALCULATE INTERNET TRAFFIC COSTS ----------------------- //
//		
//		long bytes = Math.abs(InternetTrafficManager.getTotalBytes() - InternetTrafficManager.getTotalStartBytes());
//		long monthlyBytes = DataManager.getMonthlyInternetTraffic();
//		
//		long totalBytes = bytes + monthlyBytes;
//		
//		double totalInternetCosts = Calculator.calculateInternetTrafficCost(totalBytes, tariff);
//		txtView = (TextView) findViewById(R.id.internetCostValueView);
//		txtView.setText(DataManager.toCost(totalInternetCosts));
//		
//		txtView = (TextView) findViewById(R.id.internetUsageValueView);
//		txtView.setText(DataManager.formatByteUsage(totalBytes, tariff.getFreeInternetTraffic()));
//		
//		//calculate total cost
//		double totalCost = totalMonthlyCost + totalCallCosts + totalSmsCosts + totalInternetCosts;
//		txtView = (TextView) findViewById(R.id.totalCostValueView);
//		txtView.setText(DataManager.toCost(totalCost));
//	}

	@Override
	public void run() {		
		
		//check if new number operator information was downloaded
		if (DOWNLOAD_DONE) {
			DOWNLOAD_DONE = false;
			calculateCosts();
		}
		
		long bytes = Math.abs(InternetTrafficManager.getTotalBytes() - InternetTrafficManager.getTotalStartBytes());
		long monthlyBytes = DataManager.getMonthlyInternetTraffic();
		
		long totalBytes = bytes + monthlyBytes;
		
//		Log.d("INTERNET", String.valueOf(InternetTrafficManager.getTotalBytes()/1000000) + " " + 
//						  String.valueOf(InternetTrafficManager.getTotalStartBytes() / 1000000));
		
		double totalInternetCosts = Calculator.calculateInternetTrafficCost(totalBytes, tariff);
		
		txtView = (TextView) findViewById(R.id.internetUsageValueView);
		txtView.setText(DataManager.formatByteUsage(totalBytes, tariff.getFreeInternetTraffic()));
		
		txtView = (TextView) findViewById(R.id.internetCostValueView);
		txtView.setText(DataManager.toCost(totalInternetCosts));
		
		mHandler.postDelayed(this, INTERNET_MONITOR_TIMER);		
	}
}
