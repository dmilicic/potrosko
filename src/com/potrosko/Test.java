package com.potrosko;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.potrosko.controller.DataManager;
import com.potrosko.controller.asynctasks.NumberOperatorTask;
import com.potrosko.log.manager.InternetTrafficManager;
import com.potrosko.log.manager.PhonecallLogManager;
import com.potrosko.log.manager.SmsLogManager;
import com.potrosko.log.model.AbstractLog;
import com.potrosko.log.model.PhonecallLog;
import com.potrosko.tariff.model.Tariff;

public class Test extends Activity {
	
	private ListView smsView;
	private TextView textView;
	
	SmsLogManager sms;
	PhonecallLogManager phone;
	InternetTrafficManager inet;
	
	ArrayList<String> msgData;
	ArrayList<AbstractLog> logs;
	ArrayList<Object> objects; 

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test);
		
		init();
		
//		DataManager.updateNumberOperatorMap(DataManager.getAllContactNumbers());
		NumberOperatorTask task = new NumberOperatorTask();
		try {
			task.execute(DataManager.getAllNumbers()).get();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		} catch (ExecutionException e1) {
			e1.printStackTrace();
		}
		
//		ArrayList<String> alContacts = new ArrayList<String>();
//		HashMap<String, String> numMap = DataManager.getNumberOperatorMap();
//		for(String s : numMap.keySet()) {
//			alContacts.add(s + " | " + numMap.get(s));
//		}
		

		objects = DataManager.getTariffs();
		Tariff tariff = (Tariff) objects.get(0);
		textView.setText(tariff.getNAME());

		
		logs = DataManager.getCallLogs();
				
		for(int i = 0; i < logs.size(); i++) {
			msgData.add(((PhonecallLog) logs.get(i)).getNumberDialed() + " | " + ((PhonecallLog) logs.get(i)).getDuration());
		}

		msgData.add("================");
		HashMap<String, String> map = DataManager.getNumberOperatorMap();
		for(String s : map.keySet()) {
			msgData.add(s + " | " + map.get(s));
		}
				
		smsView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, msgData));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.test, menu);
		return true;
	}
	
	public void init() {
		DataManager.context = this;
		
		smsView = (ListView) findViewById(R.id.smsListView);
		textView = (TextView) findViewById(R.id.internet);
		
		objects = new ArrayList<Object>();
		msgData = new ArrayList<String>();
		logs = new ArrayList<AbstractLog>();
		sms = new SmsLogManager(getContentResolver());
		phone = new PhonecallLogManager(getContentResolver());
		inet = InternetTrafficManager.getInstance();
	}

}
