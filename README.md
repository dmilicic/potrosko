# README

DISCLAIMER: This application was being developed for a startup Potrosko at Zagreb Entrepreneurship Incubator. 

The main goal of the application was to determine mobile costs from phone calls, sms and internet traffic and to
recommend better tariff models from different mobile operators depending on the usage of the phone. 

THIS PRODUCT IS UNFINISHED AND ALL DEVELOPMENT HAS HALTED!


TASKS
====================
- test tariff calculation
- add special premium number list 
	- http://imenik.tportal.hr/show?action=060&searchType060=name&prefixNumber=060&number=&name=tele+2
	- http://www.vipnet.hr/vip-parking
	- http://www.slsolucije.hr/vas_imenik.php
	- http://imenik.tportal.hr/show?action=060&searchType060=name&prefixNumber=060&number=&name=tele+2
- add all the tariffs
- save call and sms logs to app memory in case they get deleted by user
	- track number of sms sent to each network
- optimize calculations


Tariff definition
==================
Operator
Type
Name
Monthly cost
Call cost
Call start cost 
Call calculation unit
SMS cost
MMS cost
Internet traffic cost
Internet calculation uni
Free call networks
Free sms networks
Free call minutes
Free sms count
Free internet traffic
Discount


OPERATOR LIST
===================
Amis Telekom d.o.o.
B.net Hrvatska d.o.o.
Bonbon
evoTV
H1 Telekom d.d.
Iskon Internet d.d.
MultiPlus mobile
Optima Telekom d.d.
Simpa
T-HT Hrvatski Telekom d.d.
TELE2 d.o.o.
Terrakom d.o.o.
Tomato
VIPnet d.o.o.
